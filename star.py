def main(rows):
  offset = rows*2 +1

  # printing top traingle
  i = 1
  while i <= rows:
      j = rows
      while j+offset > i:
          print(' ', end='')
          j -= 1
      print('*', end=' ')
      k = 1
      while k < (i - 1):
          print(' ', end=' ')
          k += 1
      if i == 1:
          print()
      else:
          print('*')
      i += 1

  # Printing center shapes
  for row in range(2*rows+1):
    if row == 0 or row == 2*rows :
        line = "*"*(2*rows+1) + " "*(2*rows-1) +"*"*(2*rows+1) 
        print(line, end='')
    else :
      for col in range(6*rows-1):
        if (row == col or row+col == 2*rows) and col <= rows :
          print("*", end='')
        if (row+col == 6*rows-1 and col >= 5*rows-1) :
          print("*", end='')
        if ((col-row == 4*rows-1 and row > rows)):
          print("*", end='')
        else :
          print(end=' ')
    print("")
      
  # Printing bottom triangle
  i = rows
  while i >= 1:
      j = rows
      while j+offset > i:
          print(' ', end='')
          j -= 1
      print('*', end=' ')
      k = 1
      while k <=(i - 1)-1:
          print(' ', end=' ')
          k += 1
      if i == 1:
          print()
      else:
          print('*')
      i -= 1

# Main methode
if __name__ == "__main__":
  # size input
  rows = int(input("Please type in the size of your desired star !!")) 
  if rows >= 1 : 
    main(rows)