# KTR Epitech

KTR technical evaluation
Python script to draw a hexagram star in terminal

How to run it ?
Run it just like any other python script (python3 star.py)
A message will pop up asking for a size for the star , type in the desired size and Voila, your star is printed!
